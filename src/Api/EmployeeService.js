import axios from 'axios';

const EMPLOYEE_API_BASE_URL = 'http://localhost:8143/api/persona/';

/*const auth = {
    auth: {
        username: "crud",
        password: "d9490f44-da7e-4a38-9ed6-a1409dfdb25b",
    }
}*/


class EmployeeService {

    getEmployees() {
        return axios.get(EMPLOYEE_API_BASE_URL)

    }
    getEmployeesById(id) {
        return axios.get(EMPLOYEE_API_BASE_URL + id);
    }
    postEmployees(data) {
        return axios.post(EMPLOYEE_API_BASE_URL, data);
    }
    deleteEmployees(id) {
        return axios.delete(EMPLOYEE_API_BASE_URL + id);
    }

    putEmployees(data) {
        return axios.put(EMPLOYEE_API_BASE_URL, data);
    }
}


export default new EmployeeService();