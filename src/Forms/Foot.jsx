import React from "react";

const Foot = () => {
  return (
    <div>
      <nav className="navbar " style={{ backgroundColor: "lightblue" }}>
        <div className="container-fluid">
          <a className="navbar-brand" href="#">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAErElEQVRoge2YW2xUVRSGv3VOrygUKCrptBQEhMiDaGkRwgP4oAQeVEyjSRPk5mCAieUmhAipIYGQFIoUQxQiBWoTbqmB2OKTCYhRGDDRaChguJQOaqnSWMS2M3v50Gk7lKEcpjOlxPkfJmddzvrXn7P3OvsMxBFHHHE8TMjDbgDggofklL+tnaBjgaGZZZotoE7u7RMCAGrnSAMwGCDg14zh5Vx3cp8V06664OxC16Qz7syV9wifb79IsBjrtGavCLjgGZXsXZi136icVNh4dqHrua45itSEdDXGae1eETC69GIzaD/alqxtVEruakRMhwCD1bcEAKhlVgCtQXOa1+2aeUc8wLn2a2nbzI7QawJyd/hqEN3Z6ZFirzsnsd0yNjUh6X3vCQBYLYEioDFojoU/FrTHmpq4iOIPmtm1+aQ6qhnlHqlelJtbVBS+7gu7f6tH2Njp0Q+97qfTAMYdpAXhUkdfqYx2whdVAccW545TS45PrM87UeXJGRkuJ/XPtBLg16D5BLSs7ghq5zISh5MoagKqPKOSjSUVQIoKk1H7bPWSie6ueeMO/tyiKh+EuAp/eHf48LZL6djIBmfvgqgJSE3PDGCooHPSDFD0k6oluYeOunOGhOZO2Fm7H5WTQTMlEPBvaOumc5SKw1Ea9aPEMc+EPGOsvcgdS+B3RN+ZUXr6aLvjzALXi2rJt8Ee1KiZMrTVZwtyHADFm7VHc+/HF/VNPL3Ue6opyX5eVbbReSB7CpUj1Z68vV8vmvo4QM6uuu9QDgfjImIVJwVCRqm0nYvuh5ge5qo8E19G9TPA1e5TuGQLb08vPXXC684aCfoLkAR6OcGSKenN1zJNM+ezK/jLCUePBewufG08InMMVvn8ksPervHKwvEDk1uTtiMUhLj9CJub0m+tG+FrXAP809SSsG1a2eV/H5Q/GgJKECkMmucQ2R9Qf/mCkiMXQ/OqPHmzUbYBaSHur2ZsPzW9J/w9ElBUNDUhu3FQLTA0TPgM6D71J1bMKz1YD3D0vZxhCX67TIVpAKr65syPTx/oSQ89FFBkDWv86SVRLUD0DaB/mLQWRKoEU64DBn55hbKWvIbcpag1Zsb27+96TzwooraJDyzNT72F/1WBAoVXgMQwaTdFOGSU8rkllcedfjZ2h5hMoc9XzxzU2pyYr8hsYHIYnitzSipH9FkBodi1fFa2rfoWyjzgmTZWXT93yxfrolHfkYBlazc0AIMFGjavX9NxLNi0avEBROox+FRMrajUgfos0+/qyuLiW13r7Cl8fZKxKDCBQOn8j47UdFfbKRIc5g0GUEjvaP79ef2BfFRBQNp+AMHYt9m0anGjQh3INVAfcPU66gOtXr1lR013tWMh4C7Y9mNZAWO6S0kTSAN9ttMlgNQDT0bK2xWRC7ht+zTFzDKGLBF1ARmKDAMygEwg5R631kXKGQ4RC1i6detNoPJe8eLl7iEqdkbAJssS22VUXQJZiFyLlDMcIhZwP6zY/OkN4AbwY6w4oIcClq3dENEc37J+TdTGd6/+KxELPPICerSEorkUIsX/7wlEunFjhUf+CTgTIPpNjPsAONELHHHEEUccUcZ/VWmQM+kPQp0AAAAASUVORK5CYII=" />
          </a>
        </div>
      </nav>
    </div>
  );
};

export default Foot;
