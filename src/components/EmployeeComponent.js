import React, { useState, useEffect } from "react";
import EmployeeService from "../Api/EmployeeService.js";
import Foot from "../Forms/Foot.jsx";
import Head from "../Forms/Head.jsx";
import Tittle from "../Forms/Tittle.jsx";

function EmployeeComponent() {
  const [data, setData] = useState({});
  const [employees, setEmployees] = useState([]);
  const [busqueda, setBusqueda] = useState("");

  const viewForm = () => {
    var formm = document.getElementById("formC");
    var btn = document.getElementById("buttons");
    var ds = formm.style.display;
    var bs = btn.style.display;
    if (bs !== "" && ds !== "") {
      document.getElementById("formC").style.display = "";
      document.getElementById("buttons").style.display = "";
    } else {
      document.getElementById("formC").style.display = "none";
      document.getElementById("buttons").style.display = "none";
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.id]: e.target.value,
    });
  };

  useEffect(() => {
    getEmployees();
  }, []);

  const getEmployeesById = (e) => {
    var select = e.target;
    name = select.getAttribute("name");
    console.log("soy id", name);

    var res = {};
    //var element = "";

    //console.log("soy form",form)
    EmployeeService.getEmployeesById(name).then((response) => {
      res = response.data;
      console.log(res);
      var formm = document.getElementById("formC");
      var btn = document.getElementById("buttons");
      var ds = formm.style.display;
      var bs = btn.style.display;
      if (bs !== "" && ds !== "") {
        document.getElementById("formC").style.display = "";
        document.getElementById("buttons").style.display = "";
      } /*else {
      document.getElementById("form").style.display = "none";
      document.getElementById("buttons").style.display = "none";
    }*/
      Object.keys(res).forEach(function (ele) {
        console.log(ele);
        console.log(document.getElementById(ele));
        if (document.getElementById(ele)) {
          document.getElementById(ele).value = res[ele];
        }
      });
      document.getElementById("save").style.display = "none";
      document.getElementById("saveAndUpdate").style.display = "";
      document.getElementById("cancel").style.display = "";
    });
  };

  const postEmployees = () => {
    let confirm = window.confirm("¿Seguro que desea guardar esta información?");
    if (confirm === true) {
      if (
        data.nombre !== undefined ||
        data.apellido !== undefined ||
        data.correo !== undefined
      ) {
        console.log("soy name:::", data.nombre);

        EmployeeService.postEmployees(data).then((response) => {
          console.log("soy yo:::", data);
          EmployeeService.getEmployees().then((response) => {
            setEmployees(response.data);
          });
          alert("Informacion registrada correctamente");
          var form = document.getElementById("form");
          for (let i = 0; i < form.length; i++) {
            var element = form[i];
            var field = element.getAttribute("type");
            if (field === "text" || field === "email") {
              var label = element.getAttribute("id");
              document.getElementById(label).value = "";
            }
          }
        });
      } else {
        alert("Debe llenar todos los campos");
      }
    }
  };

  const updateEmployees = () => {
    var btn = document.getElementById("update");
    var id = btn.getAttribute("name");
    console.log("iddd", parseInt(id));
    let confirm = window.confirm(
      "¿Seguro que desea actualizar esta información?"
    );
    if (confirm === true) {
      var datos = { id: parseInt(id) };
      var form = document.getElementById("form");
      for (let i = 0; i < form.length; i++) {
        var element = form[i];
        var field = element.getAttribute("type");
        if (field === "text" || field === "email") {
          var label = element.getAttribute("id");
          var vall = element.value;
          console.log(label);
          datos[label] = vall;

          //console.log(datos);
        }
      }

      console.log(datos);

      EmployeeService.putEmployees(datos).then((response) => {
        console.log("soy yo:::", datos);

        EmployeeService.getEmployees().then((response) => {
          setEmployees(response.data);
        });
        alert("Informacion actualizada correctamente");

        var form = document.getElementById("form");
        for (let i = 0; i < form.length; i++) {
          var element = form[i];
          var field = element.getAttribute("type");
          if (field === "text" || field === "email") {
            var label = element.getAttribute("id");
            document.getElementById(label).value = "";
          }
        }
      });
      document.getElementById("save").style.display = "";
      document.getElementById("saveAndUpdate").style.display = "none";
      document.getElementById("cancel").style.display = "none";
    }
  };

  const handleCancel = () => {
    document.getElementById("save").style.display = "";
    document.getElementById("saveAndUpdate").style.display = "none";
    document.getElementById("cancel").style.display = "none";
    var form = document.getElementById("form");
    for (let i = 0; i < form.length; i++) {
      var element = form[i];
      var field = element.getAttribute("type");
      if (field === "text" || field === "email") {
        var label = element.getAttribute("id");
        document.getElementById(label).value = "";
      }
    }
  };
  

  const getEmployees = () => {
    document.getElementById("saveAndUpdate").style.display = "none";
    document.getElementById("cancel").style.display = "none";
    EmployeeService.getEmployees().then((response) => {
      setEmployees(response.data);
      console.log(response.data);
    });
  };

  var name = {};

  const deleteEmployees = (e) => {
    let confirm = window.confirm("¿Seguro que desea eliminar el registro");
    if (confirm === true) {
      var select = e.target;
      name = select.getAttribute("name");

      console.log("si se employees", name);
      EmployeeService.deleteEmployees(name).then((response) => {
        alert("Registro eliminado Correctamente");
        EmployeeService.getEmployees().then((response) => {
          setEmployees(response.data);
        });
      });
    }
  };

  const handleSearch = (e) => {
    setBusqueda(
    e.target.value);
    filtrar(e.target.value);
  };

  const filtrar = (search) => {
      var resultSearch = employees.filter((elemento) => {
        if (
          elemento.nombre
            .toString()
            .toLowerCase()
            .includes(search.toLowerCase()) 
        ) {
          return elemento;
        }
      });
      setEmployees(resultSearch);
    
    
   
   
   
  };
  

  const reload = function () {
    EmployeeService.getEmployees().then((response) => {
          setEmployees(response.data);
        });
  }
  return (
    <div style={{ backgroundColor: "" }}>
      <Head />
      <div className="container">
      <br />
        <Tittle />
        <br />
       <div className="container" style={{display:"flex"}}>

        <input
          className="form-control "
          value={busqueda}
          placeholder="Búsqueda"
          onChange={handleSearch}
          onClick={reload}
       
          id="search"
          
        />
        
       </div>
       <br />
        <br />
        <br />
        <button
          type="submit"
          className="btn btn-outline-dark"
          onClick={viewForm}
          id="register"
        >
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAA60lEQVRoge3VQU7DMBBG4TcSew5AYMdpeojufKaWVW/BXbptLwAIEacRw6qSjVqQkOUO8H87O5sZJcoDERH5z6w8bNwX5qyBmwvN8509RlqaPR4vrsqnh5kVHnZ4gAFYA3fHi2qBt4mh90Q/cFseqgVec99JWqgWeB7ZQ/i3sCsP1QIvE8nfeSDuEjs30qWHkD+lCtn95mlhWPCQedour0+HbJrmlcUdHmDAvwhZHueof5/S+ZDlfOg7SgOfP6FfEDI7H7Ixzwn30CEzN4VMWlLIOlPIwlHIOlPIpDWFrDOFLByFrDOFTESkrQ/1rnoPvnV+4AAAAABJRU5ErkJggg==" />
        </button>
        <br />
        <br />
        <div className="card">
          <div className="card-body" style={{ display: "none" }} id="formC">
            <form
              onSubmit={handleSubmit}
              id="form"
              style={{ backgroundColor: "" }}
            >
              <div className="mb-3">
                <label id="exampleInputEmail1-lbl" className="form-label">
                  Nombres
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="nombre"
                  aria-describedby="emailHelp"
                  onChange={handleChange}
                />
              </div>
              <div className="mb-3">
                <label id="apellido-lbl" className="form-label">
                  Apellidos
                </label>
                <input
                  type="text"
                  className="form-control"
                  onChange={handleChange}
                  id="apellido"
                />
              </div>
              <div className="mb-3">
                <label id="correo-lbl" className="form-label">
                  Email
                </label>
                <input
                  type="email"
                  className="form-control"
                  onChange={handleChange}
                  id="correo"
                />
              </div>
            </form>
          </div>
        </div>
        <div className="card">
          <div
            className="card-body"
            id="buttons"
            style={{ display: "none", backgroundColor: "" }}
          >
            <button
              type="submit"
              className="btn btn-primary"
              onClick={postEmployees}
              id="save"
            >
              Guardar
            </button>

            <button
              type="submit"
              className="btn btn-info"
              id="saveAndUpdate"
              onClick={updateEmployees}
            >
              Guardar y actualizar
            </button>
            <br />
            <br />

            <button
              type="submit"
              className="btn btn-warning"
              onClick={handleCancel}
              id="cancel"
            >
              Cancelar
            </button>
          </div>
        </div>
        <div className="table-responsive">
          <table className="table" style={{ display: "" }}>
            <thead style={{ display: "" }}>
              <tr>
                <th> </th>
                <th> Nombre</th>
                <th> Apellido</th>
                <th> Email</th>
                <th> Acciones</th>
              </tr>
            </thead>
            <tbody style={{ display: "" }}>
              {employees && employees.map((employee) => (
                <tr key={employee.id}>
                  <td></td>
                  <td> {employee.nombre}</td>
                  <td> {employee.apellido}</td>
                  <td> {employee.correo}</td>
                  <td>
                    {" "}
                    {
                      <button
                        type="submit"
                        className="btn btn-success"
                        onClick={getEmployeesById}
                        name={employee.id}
                        id="update"
                      >
                        Actualizar
                      </button>
                    }
                  </td>
                  <td>
                    {" "}
                    {
                      <button
                        type="submit"
                        className="btn btn-danger"
                        onClick={deleteEmployees}
                        name={employee.id}
                        id="delete"
                      >
                        Eliminar
                      </button>
                    }
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <br />
      </div>
      <Foot />
    </div>
  );
}

export default EmployeeComponent;
